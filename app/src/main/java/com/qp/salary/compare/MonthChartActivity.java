package com.qp.salary.compare;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;
import java.util.List;

public class MonthChartActivity extends AppCompatActivity {

    private final int senatorMonthlyAllowance = 13500000;
    private static String senatorMonthlyAllowanceString;
    private final static String INCOME = "income";
    private static float mIncome, months, years;

    private static int[] mColors;

    public static void start(Context c, float income, boolean clear) {
        Intent intent = new Intent(c, MonthChartActivity.class);
        if (clear) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        intent.putExtra(INCOME, income);
        c.startActivity(intent);
    }

    public static void start(Context c, float income) {
        start(c, income, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_chart);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // get extra from intent
        Bundle extras = getIntent().getExtras();

//        if (extras == null) {
//            RecentPostActivity.start(this);
//        }
        if (extras != null) {
            mIncome = extras.getFloat(INCOME);
        } else {
            onBackPressed();
        }

        mColors = new int[]{
                getResources().getColor(R.color.colorPink),
                getResources().getColor(R.color.colorPrimaryDark)
        };

        TextView result = findViewById(R.id.result);

        months = senatorMonthlyAllowance / mIncome;
        senatorMonthlyAllowanceString = MainActivity.format.format(senatorMonthlyAllowance * MainActivity.rate);

        result.setText(String.format(getString(R.string.result), Period.format(months), getString(MainActivity.currencyKey), senatorMonthlyAllowanceString));

        PieChart pieChart = findViewById(R.id.chart);
        PieData data = new PieData(getPieDataSet());
        data.setValueFormatter(new PercentFormatter());
        pieChart.setData(data);
        Description s = new Description();
        s.setText(String.format(getString(R.string.month_description), getString(MainActivity.currencyKey), senatorMonthlyAllowanceString));
        pieChart.setDescription(s);
        pieChart.invalidate(); // refresh
        pieChart.setVisibility(View.VISIBLE);
//        pieChart.setDrawEntryLabels(false);
//        pieChart.setUsePercentValues(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_month_chart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_years_chart:
                YearChartActivity.start(MonthChartActivity.this, mIncome, false);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private PieDataSet getPieDataSet() {
        List<PieEntry> entries = new ArrayList<>();

        float senatorPercentage = 1 / (months + 1) * 100;
        float incomePercentage = 100 - senatorPercentage;

        entries.add(new PieEntry(incomePercentage, "You"));
        entries.add(new PieEntry(senatorPercentage, "Senator"));

        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : mColors)
            colors.add(c);


        PieDataSet set = new PieDataSet(entries, "");
        set.setColors(colors);
//        set.setDrawValues(false);
//        set.setDrawIcons(false);
        return set;
    }
}
